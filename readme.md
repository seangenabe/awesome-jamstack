# JAMstack web development

Curated sites and tools for the development of JAMstack-powered websites.

## General

* [JAMstack](https://jamstack.org/)

## Static site generators

See also [staticgen.com](https://www.staticgen.com/)

* [GatsbyJS](https://www.gatsbyjs.org/)

## Hosting

See also [the web hosting section on my bookmarks](https://gitlab.com/seangenabe/awesome-bookmarks#web-hosting)

* [Netlify](https://netlify.com/)

## Headless CMS

See also [headlesscms.org](https://headlesscms.org/)

### API-based

| Name        | Free tier offering
|-------------|-|
| Contentful  | 5k records, 24 content types, no GraphQL
| Cosmic JS   | 10k calls, 1 GB storage
| Flamelink   | ∞ records
| Prismic     | "no hard limits" on storage / usage

## Other lists

* [The New Dynamic](https://www.thenewdynamic.org/) - interactive list of tools
* [automata/awesome-jamstack](https://github.com/automata/awesome-jamstack)

